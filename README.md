# cpp-error-factory

Build Instruction
=================

Build in a Linux machine

```
$ cmake -B build
$ cd build
$ make
$ ls cpp-error-factory*
cpp-error-factory     # Executable
cpp-error-factory.dbg # Debug symbol
$ ./cpp-error-factory
Hello World!
```

Build and run in Docker 

```
#Build
docker-compose run --rm runner bash -c "cmake /code && make"

#Run
SENTRY_DSN="your-dsn"
docker-compose run --rm -e SENTRY_DSN=${SENTRY_DSN} runner /build/cpp-error-factory
```

Validate the generated symbol via GDB

```
$ gdb cpp-error-factory
(gdb) l main
No symbol table is loaded.  Use the "file" command.
(gdb) add-symbol-file cpp-error-factory.dbg
add symbol table from file "cpp-error-factory.dbg"
(y or n) y
Reading symbols from cpp-error-factory.dbg...
(gdb) l main
1	#include <iostream>
2
3	using namespace std;
4
5	int main()
6	{
7	    cout << "Hello World!" << endl;
8	    return 0;
9	}
(gdb)
```
