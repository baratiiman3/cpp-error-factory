#include <iostream>
#include <sentry.h>
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif
using namespace std;

int main()
{
    sentry_options_t *options = sentry_options_new();
    
    /* You may set the DSN via the SENTRY_DSN env var or uncomment the line below to fill-in hardcoded value */
    // sentry_options_set_dsn(options, "YOUR_DSN"); 

    /* Set sample rate to 1.0 will capture 100%. It is recommended to adjust the rate in production env */
    sentry_options_set_sample_rate(options, 1.0);

    sentry_options_set_debug(options, 1);
    
    sentry_options_set_release(options, "cpp-error-factory@0.0.1");
    
    sentry_init(options);

    const char *msg = "Hello World!";

    cout << msg << endl;

    sentry_capture_event(sentry_value_new_message_event(
        SENTRY_LEVEL_INFO,
        "custom",
        msg
    ));
    
    sleep(2); /* Prevent to close immediately */

    sentry_close();

    return 0;
}
